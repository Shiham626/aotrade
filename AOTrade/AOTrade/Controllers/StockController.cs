﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AOTrade.Models.ViewModel;
using AOTrade.Services;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace AOTrade.Controllers
{
    public class StockController : Controller
    {
        private readonly ISettingsService _settingsService;
        private readonly ICashService _cashService;
        private readonly IMapper _mapper;

        public StockController(ISettingsService settingsService, IMapper mapper, ICashService cashService)
        {
            _settingsService = settingsService;
            _cashService = cashService;
            _mapper = mapper;
        }

        /////////////--------------------------  C R U D ---------------------------------////////////////


        #region Purchase
        #region Get Method
        public IActionResult Purchase()
        {
            return View();
        }
        [HttpGet]
        public IActionResult PurchaseForm(int id = 0)
        {
            try
            {
                var viewModel = new ExpenseVM()
                {
                    Date = DateTime.UtcNow.AddHours(6)
                };
                if (id > 0)
                {
                    viewModel = _cashService.GetExpense(id);
                    if (viewModel == null)
                    {
                        return Json(new { act = false, msg = "Data not found" });
                    }
                }
                ViewBag.CompanyId = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(_settingsService.GetCompany(), "Id", "CompanyName");
                return PartialView("PurchaseForm", viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public IActionResult PurchaseList(String from, String to)
        {
            try
            {
                var viewModel = _cashService.GetExpense();
                return PartialView("ExpenseList", viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Post Method
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SavePurchase(ExpenseVM model)
        {
            if (!ModelState.IsValid) { return Json(new { act = false, msg = "Error! Invalid input" }); }
            try
            {
                model.CreateDate = DateTime.UtcNow.AddHours(6);
                await _cashService.SaveExpenseAsync(model);
                var viewModel = _cashService.GetExpense();
                return PartialView("ExpenseList", viewModel);
            }
            catch (Exception ex)
            {
                return Json(new { act = false, msg = "Error! Data not saved" });
            }
        }

        [HttpPost]
        public async Task<JsonResult> PurchaseDelete(int id)
        {
            try
            {
                await _cashService.DeleteExpenseAsync(id);
                return Json(new { act = true, msg = "Deleted! Changes saved successfully" });
            }
            catch (Exception ex)
            {
                //throw ex;
                return Json(new { act = false, msg = "Error! Error occured" });
            }
        }
        #endregion
        #endregion

        #region Sale
        #region Get Method
        public IActionResult Sale()
        {
            return View();
        }
        [HttpGet]
        public IActionResult SaleForm(int id = 0)
        {
            try
            {
                var viewModel = new ExpenseVM()
                {
                    Date = DateTime.UtcNow.AddHours(6)
                };
                if (id > 0)
                {
                    viewModel = _cashService.GetExpense(id);
                    if (viewModel == null)
                    {
                        return Json(new { act = false, msg = "Data not found" });
                    }
                }
                ViewBag.CompanyId = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(_settingsService.GetCompany(), "Id", "CompanyName");
                return PartialView("SaleForm", viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public IActionResult SaleList(String from, String to)
        {
            try
            {
                var viewModel = _cashService.GetExpense();
                return PartialView("SaleList", viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Post Method
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveSale(ExpenseVM model)
        {
            if (!ModelState.IsValid) { return Json(new { act = false, msg = "Error! Invalid input" }); }
            try
            {
                model.CreateDate = DateTime.UtcNow.AddHours(6);
                await _cashService.SaveExpenseAsync(model);
                var viewModel = _cashService.GetExpense();
                return PartialView("ExpenseList", viewModel);
            }
            catch (Exception ex)
            {
                return Json(new { act = false, msg = "Error! Data not saved" });
            }
        }

        [HttpPost]
        public async Task<JsonResult> SaleDelete(int id)
        {
            try
            {
                await _cashService.DeleteExpenseAsync(id);
                return Json(new { act = true, msg = "Deleted! Changes saved successfully" });
            }
            catch (Exception ex)
            {
                //throw ex;
                return Json(new { act = false, msg = "Error! Error occured" });
            }
        }
        #endregion
        #endregion
    }
}