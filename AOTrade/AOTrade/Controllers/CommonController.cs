﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AOTrade.Models.Entity;
using AOTrade.Models.ViewModel;
using AOTrade.Services;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace AOTrade.Controllers
{
    public class CommonController : Controller
    {

        private readonly ISettingsService _settingsService;
        private readonly ICashService _cashService;
        private readonly IMapper _mapper;

        public CommonController(ISettingsService settingsService, IMapper mapper, ICashService cashService)
            {
            _settingsService = settingsService;
            _cashService = cashService;
            _mapper = mapper;
        }

        /////////////--------------------------  C R U D ---------------------------------////////////////


        #region Item Group to find Item
        [HttpGet]
        public IActionResult GetItemList(int id = 0)
        {
            try
            {
                var viewModel = new ExpenseVM();
                //var viewdt = 
                ViewBag.ItemId = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(_settingsService.GetItem().Where(m => m.GroupId == id).ToList(), "Id", "ItemName");
                return PartialView("GetItemList", viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region Item Name to find description
        [HttpGet]
        public IActionResult GetItemDescription(int id = 0)
        {
            try
            {
                var viewModel = new ExpenseVM();
                //var viewdt = 
                ViewBag.ItemDescription = _settingsService.GetItem(id).Description;
                return PartialView("GetItemDescription");
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        ////////////-----------------------------------------------/////////////////////

        #region Private method
        private void ItemOnlyViewBag(string title, ExpenseVM model = null)
        {
            //TODO: UT
            try
            {
                ViewBag.IsModelValid = true;
                ViewBag.Title = title;
                if (model == null)
                {
                    ViewBag.ItemId = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(_settingsService.GetItem(), "Id", "ItemName");
                }
                else
                {
                    ViewBag.ItemId = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(_settingsService.GetItem(), "Id", "ItemName", model.ItemId);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}