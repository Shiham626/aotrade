﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AOTrade.Models.ViewModel;
using AOTrade.Services;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AOTrade.Controllers
{
    public class PayrollController : Controller
    {
        private readonly ISettingsService _settingsService;
        private readonly IMapper _mapper;
        private readonly ICashService _cashService;
        private readonly IPayrollService _payrollService;
        private readonly IHostingEnvironment _hostingEnvironment;
        //private readonly IHttpContextAccessor _contextAccessor;


        public PayrollController(ISettingsService settingsService, IMapper mapper, ICashService cashService, IPayrollService payrollService, IHostingEnvironment hostingEnvironment)
        {
            _settingsService = settingsService;
            _cashService = cashService;
            _payrollService = payrollService;
            _hostingEnvironment = hostingEnvironment;
            //_contextAccessor = contextAccessor;
            _mapper = mapper;
        }
        /////////////--------------------------  C R U D ---------------------------------////////////////


        #region Employee
        #region Get Method
        public IActionResult Employee()
        {
            return View();
        }

        [HttpGet]
        public IActionResult EmployeeForm(int id = 0)
        {
            try
            {
                var viewModel = new EmployeeVM();
                if (id > 0)
                {
                    viewModel = _payrollService.GetEmployee(id);
                    if (viewModel == null)
                    {
                        return Json(new { act = false, msg = "Data not found" });
                    }
                }
                return PartialView("EmployeeForm", viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public IActionResult EmployeeList(int eId)
        {
            try
            {
                var viewModel = _payrollService.GetEmployee();
                return PartialView("EmployeeList", viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Post Method
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveEmployee(EmployeeVM model, IFormFile photoFile)
        {
            if (!ModelState.IsValid) { return Json(new { act = false, msg = "Error! Invalid input" }); }
            try
            {
                model.EmpNameId = model.Id +", "+ model.EmpName;

                int nid = model.Id;
                if (model.Id == 0){
                    model.BeginSalary = (decimal)model.Salary;

                    var yr = Convert.ToDateTime(DateTime.UtcNow.AddHours(6)).Year.ToString();
                    int year = Int32.Parse(yr.Substring(yr.Length - 2));
                    var t = _payrollService.GetEmployee().Max(u => (int?)u.Id).ToString();
                    if (t == null || t == ""){t = "0000";}
                    int ids = Int32.Parse(t.Substring(t.Length - 3)) + 1;
                    nid = year * 1000 + ids;
                    model.empNewId = nid;
                }

                var hostPath = _hostingEnvironment.WebRootPath;
                var dataPath = "/images/";
                var savepath = hostPath + dataPath;
                int height =450;
                int width = 350;
                var ext = ".png";
                _payrollService.Comm_ImageFormatForStoreWithNameAndExt(nid.ToString(), photoFile, savepath, height, width, ext);

                model.Photo = dataPath + model.Id.ToString() + ext;
                model.CreateDate = DateTime.UtcNow.AddHours(6);

                await _payrollService.SaveEmployeeAsync(model);

                var viewModel = _payrollService.GetEmployee();
                return PartialView("EmployeeList", viewModel);
            }
            catch (Exception ex)
            {
                return Json(new { act = false, msg = "Error! Data not saved" });
            }
        }

        [HttpPost]
        public async Task<JsonResult> EmployeeDelete(int id)
        {
            try
            {
                await _payrollService.DeleteEmployeeAsync(id);
                return Json(new { act = true, msg = "Deleted! Changes saved successfully" });
            }
            catch (Exception ex)
            {
                //throw ex;
                return Json(new { act = false, msg = "Error! Error occured" });
            }
        }
        #endregion
        #endregion

        /////---------------------//  BONUS   //-----------------------------///////////

        #region Bonus
        #region Get Method
        public IActionResult Bonus()
        {
            return View();
        }

        [HttpGet]
        public IActionResult EmpBonusForm(int id = 0)
        {
            try
            {
                var viewModel = new BonusRecordVM();

                //List<EmployeeVM> EmpList = _payrollService.GetEmployee();

                //viewModel.EmployeeVMList = EmpList;

                //if (id > 0)
                //{
                //    if (viewModel == null)
                //    {
                //        return Json(new { act = false, msg = "Data not found" });
                //    }
                //}
                return PartialView("EmpBonusForm", viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //[HttpGet]
        //public IActionResult EmployeeList(String from, String to)
        //{
        //    try
        //    {
        //        var viewModel = _payrollService.GetEmployee();
        //        return PartialView("EmployeeList", viewModel);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        #endregion

        #region Post Method
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SaveEmpBonus(BonusRecordVM model)
        {
            if (!ModelState.IsValid) { return Json(new { act = false, msg = "Error! Invalid input" }); }
            try
            {
                var viewModel = model;

                viewModel.EmployeeVMList = _payrollService.GetEmployee();

                decimal pers = 0;
                if (model.BonusPercent != null)
                {
                    pers = (decimal)model.BonusPercent;
                }
                

                foreach (var item in viewModel.EmployeeVMList)
                {
                    // Here begin salary used as Bonus Amount :D 

                    item.BeginSalary = (decimal)(item.Salary * (pers / 100));
                }


                return PartialView("EmpBonusList", viewModel);
            }
            catch (Exception ex)
            {
                return Json(new { act = false, msg = "Error! Data not saved" });
            }
        }

        //[HttpPost]
        //public async Task<JsonResult> EmpBonusDelete(int id)
        //{
        //    try
        //    {
        //        await _payrollService.DeleteEmployeeAsync(id);
        //        return Json(new { act = true, msg = "Deleted! Changes saved successfully" });
        //    }
        //    catch (Exception ex)
        //    {
        //        //throw ex;
        //        return Json(new { act = false, msg = "Error! Error occured" });
        //    }
        //}
        #endregion
        #endregion

        /////---------------------//  Employee Salary   //-----------------------------///////////

        #region Employee Salary
        #region Get Method
        public IActionResult EmpSalary()
        {
            return View();
        }

        [HttpGet]
        public IActionResult EmpSalaryForm(int id = 0)
        {
            try
            {
                var viewModel = new EmpSalaryVM {
                    Date = DateTime.UtcNow.AddHours(6)
                };
                viewModel.EmployeeVMList = _payrollService.GetEmployee();
                if (id > 0)
                {
                    viewModel = _payrollService.GetEmpSalary(id);
                    if (viewModel == null)
                    {
                        return Json(new { act = false, msg = "Data not found" });
                    }
                    viewModel.EmployeeVMList = _payrollService.GetEmployee().Where(m => m.Id == viewModel.EmpId).ToList();
                    viewModel.EmpSalaryVMList = _payrollService.GetEmpSalary().Where(m => m.Id == id).ToList();
                    viewModel.EmpSalaryVMList[0].EmpId = viewModel.EmpId;
                    viewModel.EmpSalaryVMList[0].AbsentDay = viewModel.AbsentDay;
                }
                return PartialView("EmpSalaryForm", viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public IActionResult EmpSalaryList(int eId)
        {
            try
            {
                var viewModel = _payrollService.GetEmpSalary();
                return PartialView("EmpSalaryList", viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Post Method
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveEmpSalary(EmpSalaryVM model)
        {
            //if (!ModelState.IsValid) { return Json(new { act = false, msg = "Error! Invalid input" }); }
            
            try
            {
                foreach (var item in model.EmpSalaryVMList)
                {
                    model.EmpId = item.EmpId;
                    model.LoanDueAmmount = item.LoanDueAmmount;
                    model.LoanDeduct = item.LoanDeduct;
                    model.AbsentDay = item.AbsentDay;
                    model.AbsentDeduct = item.AbsentDeduct;
                    model.Deduction = item.Deduction;
                    model.GrossSalary = item.GrossSalary;
                    model.Date = item.Date;
                    model.CreateDate = DateTime.UtcNow.AddHours(6);

                    await _payrollService.SaveEmpSalaryAsync(model);
                }


                var viewModel = _payrollService.GetEmpSalary();
                return PartialView("EmpSalaryList", viewModel);
            }
            catch (Exception ex)
            {
                return Json(new { act = false, msg = "Error! Data not saved" });
            }
        }

        [HttpPost]
        public async Task<JsonResult> EmpSalaryDelete(int id)
        {
            try
            {
                await _payrollService.DeleteEmpSalaryAsync(id);
                return Json(new { act = true, msg = "Deleted! Changes saved successfully" });
            }
            catch (Exception ex)
            {
                //throw ex;
                return Json(new { act = false, msg = "Error! Error occured" });
            }
        }
        #endregion
        #endregion

        /////---------------------//  Employee Loan   //-----------------------------///////////

        #region Employee Loan
        #region Get Method
        public IActionResult EmpLoan()
        {
            return View();
        }

        [HttpGet]
        public IActionResult EmpLoanForm(int id = 0)
        {
            try
            {
                var viewModel = new EmpLoanVM
                {
                    Date = DateTime.UtcNow.AddHours(6),
                    DeductDate = DateTime.UtcNow.AddHours(6)
                };
                if (id > 0)
                {
                    viewModel = _payrollService.GetEmpLoan(id);
                    if (viewModel == null)
                    {
                        return Json(new { act = false, msg = "Data not found" });
                    }
                }
                ViewBag.EmpId = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(_payrollService.GetEmployee(), "Id", "EmpNameId", viewModel.EmpId);
                return PartialView("EmpLoanForm", viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public IActionResult EmpLoanList(int eId)
        {
            try
            {
                var viewModel = _payrollService.GetEmpLoan();
                return PartialView("EmpLoanList", viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Post Method
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveEmpLoan(EmpLoanVM model)
        {
            if (!ModelState.IsValid) { return Json(new { act = false, msg = "Error! Invalid input" }); }

            try
            {
                if(model.Id == 0)
                {
                    model.DueAmmount = model.LoanAmmount;
                }
                if (model.Id > 0)
                {
                    var currentLoan = _payrollService.GetEmpLoan(model.Id);
                    model.DueAmmount = currentLoan.DueAmmount + ((decimal)(model.LoanAmmount - currentLoan.LoanAmmount));
                }

                await _payrollService.SaveEmpLoanAsync(model);
                var viewModel = _payrollService.GetEmpLoan();
                return PartialView("EmpLoanList", viewModel);
            }
            catch (Exception ex)
            {
                return Json(new { act = false, msg = "Error! Data not saved" });
            }
        }

        [HttpPost]
        public async Task<JsonResult> EmpLoanDelete(int id)
        {
            try
            {
                await _payrollService.DeleteEmpLoanAsync(id);
                return Json(new { act = true, msg = "Deleted! Changes saved successfully" });
            }
            catch (Exception ex)
            {
                //throw ex;
                return Json(new { act = false, msg = "Error! Error occured" });
            }
        }
        #endregion
        #endregion
    }
}