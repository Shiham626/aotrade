﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AOTrade.Models.Entity;
using AOTrade.Models.ViewModel;
using AOTrade.Services;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace AOTrade.Controllers
{
    public class CashController : Controller
    {
        private readonly ISettingsService _settingsService;
        private readonly IMapper _mapper;
        private readonly ICashService _cashService;

        public CashController(ISettingsService settingsService, IMapper mapper, ICashService cashService)
        {
            _settingsService = settingsService;
            _cashService = cashService;
            _mapper = mapper;
        }
        /////////////--------------------------  C R U D ---------------------------------////////////////

        #region Expense
        #region Get Method
        public IActionResult Expense()
        {
            return View();
        }
        [HttpGet]
        public IActionResult ExpenseForm(int id = 0)
        {
            try
            {
                var viewModel = new ExpenseVM()
                {
                    Date = DateTime.UtcNow.AddHours(6)
                };
                if (id > 0)
                {
                    viewModel = _cashService.GetExpense(id);
                    if (viewModel == null)
                    {
                        return Json(new { act = false, msg = "Data not found" });
                    }
                }
                ViewBag.ItemId = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(_settingsService.GetItem()
                    .Where(m => m.GroupId == viewModel.ItemGroupId).ToList(), "Id", "ItemName");
                ViewBag.ItemGroupId = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(_settingsService.GetItemGroup(), "Id", "GroupName", viewModel.ItemGroupId);
                return PartialView("ExpenseForm", viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public IActionResult ExpenseList(String from, String to)
        {
            try
            {
                var viewModel = _cashService.GetExpense();
                return PartialView("ExpenseList", viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Post Method
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveExpense(ExpenseVM model)
        {
            if (!ModelState.IsValid) { return Json(new { act = false, msg = "Error! Invalid input" }); }
            try
            {
                model.CreateDate = DateTime.UtcNow.AddHours(6);
                await _cashService.SaveExpenseAsync(model);
                    var viewModel = _cashService.GetExpense();
                    return PartialView("ExpenseList", viewModel);
            }
            catch (Exception ex)
            {
                return Json(new { act = false, msg = "Error! Data not saved" });
            }
        }

        [HttpPost]
        public async Task<JsonResult> ExpenseDelete(int id)
        {
            try
            {
                await _cashService.DeleteExpenseAsync(id);
                return Json(new { act = true, msg = "Deleted! Changes saved successfully" });
            }
            catch (Exception ex)
            {
                //throw ex;
                return Json(new { act = false, msg = "Error! Error occured" });
            }
        }
        #endregion
        #endregion

        #region Income
        #region Get Method
        public IActionResult Income()
        {
            return View();
        }

        [HttpGet]
        public IActionResult IncomeForm(int id = 0)
        {
            try
            {
                var viewModel = new IncomeVM()
                {
                    Date = DateTime.UtcNow.AddHours(6)
                };
                if (id > 0)
                {
                    viewModel = _cashService.GetIncome(id);
                    if (viewModel == null)
                    {
                        return Json(new { act = false, msg = "Data not found" });
                    }
                }
                SetIncomeViewBag("Income", viewModel);
                return PartialView("IncomeForm", viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public IActionResult IncomeList(String from, String to)
        {
            try
            {
                var viewModel = _cashService.GetIncome();
                return PartialView("IncomeList", viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Post Method
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveIncome(IncomeVM model)
        {
            if (!ModelState.IsValid) { return Json(new { act = false, msg = "Error! Invalid input" }); }
            try
            {
                model.CreateDate = DateTime.UtcNow.AddHours(6);
                await _cashService.SaveIncomeAsync(model);

                var viewModel = _cashService.GetIncome();
                return PartialView("IncomeList", viewModel);
            }
            catch (Exception ex)
            {
                return Json(new { act = false, msg = "Error! Data not saved" });
            }
        }

        [HttpPost]
        public async Task<JsonResult> IncomeDelete(int id)
        {
            try
            {
                await _cashService.DeleteIncomeAsync(id);
                return Json(new { act = true, msg = "Deleted! Changes saved successfully" });
            }
            catch (Exception ex)
            {
                //throw ex;
                return Json(new { act = false, msg = "Error! Error occured" });
            }
        }
        #endregion
        #endregion






        ////////////-----------------------------------------------/////////////////////

        #region Private method



        private void SetIncomeViewBag(string title, IncomeVM model)
        {
            //TODO: UT
            try
            {
                ViewBag.IsModelValid = true;
                ViewBag.Title = title;
                ViewBag.ItemGroupId = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(_settingsService.GetItemGroup(), "Id", "GroupName", model.ItemGroupId);
                ViewBag.ItemId = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(_settingsService.GetItem().Where(m => m.GroupId == model.ItemGroupId).ToList(), "Id", "ItemName");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}