﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AOTrade.Models;

namespace AOTrade.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Home()
        {
            return View();
        }



        #region Post Method
        [HttpPost]
        public IActionResult Request()
        {
            try
            {
                return RedirectToAction("Employee","Payroll");
            }
            catch (Exception ex)
            {
                return Json(new { act = false, msg = "Error! Data not saved" });
            }
        }

        #endregion


        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
