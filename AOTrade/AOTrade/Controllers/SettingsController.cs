﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AOTrade.Models.Entity;
using AOTrade.Models.ViewModel;
using AOTrade.Services;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace AOTrade.Controllers
{
    public class SettingsController : Controller
    {

        private readonly ISettingsService _settingsService;
        private readonly IMapper _mapper;

        public SettingsController(ISettingsService settingsService, IMapper mapper)
            {
            _settingsService = settingsService;
            _mapper = mapper;
        }

        /////////////--------------------------  C R U D ---------------------------------////////////////


        #region Item Group
        #region Get Method
        public IActionResult ItemGroup()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ItemGroupForm(int id = 0)
        {
            try
            {
                var viewModel = new ItemGroupVM();
                if (id > 0)
                {
                    viewModel = _settingsService.GetItemGroup(id);
                    if (viewModel == null) {
                        return Json(new { act = false, msg = "Data not found" });
                    }
                }
                ViewBag.Type = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(DropDownListUtility.GetCashTypeDropdown("Direct"), "Value", "Text", viewModel.Type);
                return PartialView("ItemGroupForm", viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public IActionResult ItemGroupList(String from, String to)
        {
            try
            {
                var viewModel = _settingsService.GetItemGroup();
                return PartialView("ItemGroupList", viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region Post Method
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveItemGroup(ItemGroupVM model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { act = false, msg = "Error! Invalid input" });
            }
            try
            {
                model.CreateDate = DateTime.UtcNow.AddHours(6);
                await _settingsService.SaveItemGroupAsync(model);

                var viewModel = _settingsService.GetItemGroup();
                return PartialView("ItemGroupList", viewModel);
            }
            catch (Exception ex)
            {
                return Json(new { act = false, msg = "Error! Data not saved" });
            }
        }


        [HttpPost]
        public async Task<JsonResult> ItemGroupDelete(int id)
        {
            try
            {
                await _settingsService.DeleteItemGroupAsync(id);
                return Json(new { act = true, msg = "Deleted! Changes saved successfully" });
            }
            catch (Exception ex)
            {
                //throw ex;
                return Json(new { act = false, msg = "Error! Error occured" });
            }
        }
        #endregion
        #endregion


        /////////////////////////////////////////////////////// ----------  Item  Name  -------------- ///////////////////////////////////////////////////////////////
        #region Item Name
        #region Get Method
        public IActionResult Item()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ItemForm(int id = 0)
        {
            try
            {
                var viewModel = new ItemVM();
                if (id > 0)
                {
                    viewModel = _settingsService.GetItem(id);
                    if (viewModel == null)
                    {
                        return Json(new { act = false, msg = "Data not found" });
                    }
                }
                SetItemViewBag("Item", viewModel);
                return PartialView("ItemForm", viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public IActionResult ItemList(String from, String to)
        {
            try
            {
                var viewModel = _settingsService.GetItem();
                return PartialView("ItemList", viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region Post Method
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveItem(ItemVM model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { act = false, msg = "Error! Invalid input" });
            }
            try
            {
                model.CreateDate = DateTime.UtcNow.AddHours(6);
                await _settingsService.SaveItemAsync(model);

                var viewModel = _settingsService.GetItem();
                return PartialView("ItemList", viewModel);
            }
            catch (Exception ex)
            {
                return Json(new { act = false, msg = "Error! Data not saved" });
            }
        }


        [HttpPost]
        public async Task<JsonResult> ItemDelete(int id)
        {
            try
            {
                await _settingsService.DeleteItemAsync(id);
                return Json(new { act = true, msg = "Deleted! Changes saved successfully" });
            }
            catch (Exception ex)
            {
                //throw ex;
                return Json(new { act = false, msg = "Error! Error occured" });
            }
        }
        #endregion
        #endregion

        /////////////////////////////////////////////////////// ----------  Company  -------------- ///////////////////////////////////////////////////////////////
        #region Company Name
        #region Get Method
        public IActionResult Company()
        {
            return View();
        }

        [HttpGet]
        public IActionResult CompanyForm(int id = 0)
        {
            try
            {
                var viewModel = new CompanyVM();
                if (id > 0)
                {
                    viewModel = _settingsService.GetCompany(id);
                    if (viewModel == null)
                    {
                        return Json(new { act = false, msg = "Data not found" });
                    }
                }
                return PartialView("CompanyForm", viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public IActionResult CompanyList()
        {
            try
            {
                var viewModel = _settingsService.GetCompany();
                return PartialView("CompanyList", viewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region Post Method
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveCompany(CompanyVM model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { act = false, msg = "Error! Invalid input" });
            }
            try
            {
                model.CreateDate = DateTime.UtcNow.AddHours(6);
                await _settingsService.SaveCompanyAsync(model);

                var viewModel = _settingsService.GetCompany();
                return PartialView("CompanyList", viewModel);
            }
            catch (Exception ex)
            {
                return Json(new { act = false, msg = "Error! Data not saved" });
            }
        }


        [HttpPost]
        public async Task<JsonResult> CompanyDelete(int id)
        {
            try
            {
                await _settingsService.DeleteCompanyAsync(id);
                return Json(new { act = true, msg = "Deleted! Changes saved successfully" });
            }
            catch (Exception ex)
            {
                //throw ex;
                return Json(new { act = false, msg = "Error! Error occured" });
            }
        }
        #endregion
        #endregion











        ////////////-----------------------------------------------/////////////////////

        #region Private method
        private void SetItemViewBag(string title, ItemVM model = null)
        {
            //TODO: UT
            try
            {
                ViewBag.IsModelValid = true;
                ViewBag.Title = title;
                if (model == null)
                {
                    ViewBag.GroupId = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(_settingsService.GetItemGroup(), "Id", "GroupName");
                }
                else
                {
                    ViewBag.GroupId = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(_settingsService.GetItemGroup(), "Id", "GroupName", model.GroupId);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}