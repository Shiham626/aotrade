﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AOTrade.Models.Entity;
using AOTrade.Models.ViewModel;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace AOTrade.Services
{
    public class SettingsService : ISettingsService
    {
        private readonly aotradeDBContext _context;
        private readonly IMapper _mapper;
        //Constructor
        public SettingsService(aotradeDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        #region Item Group
        public List<ItemGroupVM> GetItemGroup()
        {
            try
            {
                var dataList = _context.ItemGroup.ToList();
                var modelList = Mapper.Map<List<ItemGroupVM>>(dataList);
                return modelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ItemGroupVM GetItemGroup(int id = 0)
        {
            try
            {
                var dataList =  _context.ItemGroup.AsNoTracking().FirstOrDefault(t => t.Id == id);
                var modelList = Mapper.Map<ItemGroupVM>(dataList);
                return modelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task SaveItemGroupAsync(ItemGroupVM model)
        {
            try
            {
                var entity = Mapper.Map<ItemGroup>(model);

                if (model.Id == 0)
                {
                    _context.Add(entity);
                }
                else
                {
                   _context.Update(entity); ;
                }
               await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task DeleteItemGroupAsync(int id)
        {
            try
            {
                var ItemGroup =  _context.ItemGroup.FindAsync(id).Result;
                _context.ItemGroup.Remove(ItemGroup);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Item Name
        public List<ItemVM> GetItem()
        {
            try
            {
                var dataList = _context.Item.Include(inc => inc.Group).ToList();
                var modelList = _mapper.Map<List<Item>, List<ItemVM>>(dataList);
                //foreach (var data in dataList)
                //{
                //    var VM = new ItemVM
                //    {
                //        GroupVM = data.Group
                //    }

                //}
                


                return modelList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public ItemVM GetItem(int id = 0)
        {
            try
            {
                var dataList = _context.Item.AsNoTracking().FirstOrDefault(t => t.Id == id);
                var modelList = Mapper.Map<ItemVM>(dataList);
                return modelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task SaveItemAsync(ItemVM model)
        {
            try
            {
                var entity = Mapper.Map<Item>(model);
                if (model.Id == 0)
                {
                    _context.Add(entity);
                }
                else
                {
                    _context.Update(entity);
                }
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task DeleteItemAsync(int id)
        {
            try
            {
                var Item = _context.Item.FindAsync(id).Result;
                _context.Item.Remove(Item);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Company
        public List<CompanyVM> GetCompany()
        {
            try
            {
                var dataList = _context.Company.ToList();
                var modelList = Mapper.Map<List<CompanyVM>>(dataList);
                return modelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CompanyVM GetCompany(int id)
        {
            try
            {
                var dataList = _context.Company.AsNoTracking().FirstOrDefault(t => t.Id == id);
                var modelList = Mapper.Map<CompanyVM>(dataList);
                return modelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task SaveCompanyAsync(CompanyVM model)
        {
            try
            {
                var entity = Mapper.Map<Company>(model);

                if (model.Id == 0)
                {
                    _context.Add(entity);
                }
                else
                {
                    _context.Update(entity);
                }
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task DeleteCompanyAsync(int id)
        {
            try
            {
                var Company = _context.Company.FindAsync(id).Result;
                _context.Company.Remove(Company);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
