﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AOTrade.Models.Entity;
using AOTrade.Models.ViewModel;

namespace AOTrade.Services
{
   public interface ISettingsService
    {
        #region Item Group
        ItemGroupVM GetItemGroup(int id);
        List<ItemGroupVM> GetItemGroup();
        Task SaveItemGroupAsync(ItemGroupVM model);
        Task DeleteItemGroupAsync(int id);
        #endregion

        #region Item Name
        ItemVM GetItem(int id);
        List<ItemVM> GetItem();
        Task SaveItemAsync(ItemVM model);
        Task DeleteItemAsync(int id);
        #endregion

        #region Company Name
        CompanyVM GetCompany(int id);
        List<CompanyVM> GetCompany();
        Task SaveCompanyAsync(CompanyVM model);
        Task DeleteCompanyAsync(int id);
        #endregion
    }
}
