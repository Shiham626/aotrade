﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AOTrade.Models.Entity;
using AOTrade.Models.ViewModel;

namespace AOTrade.Services
{
   public interface ICashService
    {
        #region Expense
        ExpenseVM GetExpense(int id);
        List<ExpenseVM> GetExpense();
        Task SaveExpenseAsync(ExpenseVM model);
        Task DeleteExpenseAsync(int id);
        #endregion

        #region Income
        IncomeVM GetIncome(int id);
        List<IncomeVM> GetIncome();
        Task SaveIncomeAsync(IncomeVM model);
        Task DeleteIncomeAsync(int id);
        #endregion
    }
}
