﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AOTrade.Models.Entity;
using AOTrade.Models.ViewModel;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace AOTrade.Services
{
    public class CashService : ICashService
    {
        private readonly aotradeDBContext _context;
        private readonly IMapper _mapper;
        //Constructor
        public CashService(aotradeDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        #region Ecpense
        public List<ExpenseVM> GetExpense()
        {
            try
            {
                var dataList = _context.Expense.Include(inc => inc.ItemGroup).Include(inc => inc.Item).ToList();
                var modelList = Mapper.Map<List<ExpenseVM>>(dataList);
                return modelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ExpenseVM GetExpense(int id = 0)
        {
            try
            {
                var dataList =  _context.Expense.AsNoTracking().FirstOrDefault(t => t.Id == id);
                var modelList = Mapper.Map<ExpenseVM>(dataList);
                return modelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task SaveExpenseAsync(ExpenseVM model)
        {
            try
            {
                var entity = Mapper.Map<Expense>(model);

                if (model.Id == 0)
                {
                    _context.Add(entity);
                }
                else
                {
                   _context.Update(entity); ;
                }
               await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task DeleteExpenseAsync(int id)
        {
            try
            {
                var Expense =  _context.Expense.FindAsync(id).Result;
                _context.Expense.Remove(Expense);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region Income
        public List<IncomeVM> GetIncome()
        {
            try
            {
                var dataList = _context.Income.Include(inc => inc.ItemGroup).Include(inc => inc.Item).ToList();
                var modelList = Mapper.Map<List<IncomeVM>>(dataList);
                return modelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IncomeVM GetIncome(int id = 0)
        {
            try
            {
                var dataList = _context.Income.AsNoTracking().FirstOrDefault(t => t.Id == id);
                var modelList = Mapper.Map<IncomeVM>(dataList);
                return modelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task SaveIncomeAsync(IncomeVM model)
        {
            try
            {
                var entity = Mapper.Map<Income>(model);

                if (model.Id == 0)
                {
                    _context.Add(entity);
                }
                else
                {
                    _context.Update(entity); ;
                }
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task DeleteIncomeAsync(int id)
        {
            try
            {
                var Income = _context.Income.FindAsync(id).Result;
                _context.Income.Remove(Income);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
