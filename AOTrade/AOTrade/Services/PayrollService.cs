﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AOTrade.Models.Entity;
using AOTrade.Models.ViewModel;using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace AOTrade.Services
{
    public class PayrollService : IPayrollService
    {
        private readonly aotradeDBContext _context;
        private readonly IMapper _mapper;
        //Constructor
        public PayrollService(aotradeDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        #region Employee 
        public List<EmployeeVM> GetEmployee()
        {
            try
            {
                var dataList = _context.Employee.ToList();
                var modelList = Mapper.Map<List<EmployeeVM>>(dataList);
                return modelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public EmployeeVM GetEmployee(int id = 0)
        {
            try
            {
                var dataList =  _context.Employee.AsNoTracking().FirstOrDefault(t => t.Id == id);
                var modelList = Mapper.Map<EmployeeVM>(dataList);
                return modelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task SaveEmployeeAsync(EmployeeVM model)
        {
            try
            {
                var entity = Mapper.Map<Employee>(model);

                if (model.Id == 0)
                {
                    entity.Id = model.empNewId;
                    _context.Add(entity);
                }
                else
                {
                   _context.Update(entity);
                }
               await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task DeleteEmployeeAsync(int id)
        {
            try
            {
                var Employee =  _context.Employee.FindAsync(id).Result;
                _context.Employee.Remove(Employee);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region Employee Salary
        public List<EmpSalaryVM> GetEmpSalary()
        {
            try
            {
                var dataList = _context.EmpSalary.Include(inc => inc.Emp).ToList();
                var modelList = Mapper.Map<List<EmpSalaryVM>>(dataList);
                return modelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public EmpSalaryVM GetEmpSalary(int id)
        {
            try
            {
                var dataList = _context.EmpSalary.AsNoTracking().FirstOrDefault(t => t.Id == id);
                var modelList = Mapper.Map<EmpSalaryVM>(dataList);
                return modelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task SaveEmpSalaryAsync(EmpSalaryVM model)
        {
            try
            {
                var entity = Mapper.Map<EmpSalary>(model);

                if (model.Id == 0)
                {
                    _context.Add(entity);
                }
                else
                {
                    _context.Update(entity);
                }
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task DeleteEmpSalaryAsync(int id)
        {
            try
            {
                var EmpSalary = _context.EmpSalary.FindAsync(id).Result;
                _context.EmpSalary.Remove(EmpSalary);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region Employee Loan
        public List<EmpLoanVM> GetEmpLoan()
        {
            try
            {
                var dataList = _context.EmpLoan.Include(inc => inc.Emp).ToList();
                var modelList = Mapper.Map<List<EmpLoanVM>>(dataList);
                return modelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public EmpLoanVM GetEmpLoan(int id)
        {
            try
            {
                var dataList = _context.EmpLoan.AsNoTracking().FirstOrDefault(t => t.Id == id);
                var modelList = Mapper.Map<EmpLoanVM>(dataList);
                return modelList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task SaveEmpLoanAsync(EmpLoanVM model)
        {
            try
            {
                var entity = Mapper.Map<EmpLoan>(model);

                if (model.Id == 0)
                {
                    _context.Add(entity);
                }
                else
                {
                    _context.Update(entity);
                }
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task DeleteEmpLoanAsync(int id)
        {
            try
            {
                var EmpLoan = _context.EmpLoan.FindAsync(id).Result;
                _context.EmpLoan.Remove(EmpLoan);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion





        #region Image
        public bool Comm_ImageFormatForStoreWithNameAndExt(string name, IFormFile file, string savePath, int height, int width, string ext)
        {
            try
            {

                //[NOTE: Convert FileType into ImageType]
                Image img = Image.FromStream(file.OpenReadStream(), true, true);

                //[NOTE: Create Directory]
                string fullPath = Path.Combine(savePath);
                Directory.CreateDirectory(fullPath);


                //[NOTE: Creating empty canvas]
                var draw_NewImage = new Bitmap(height, width);

                //[NOTE: Drawing image inside empty canvas]
                using (var g = Graphics.FromImage(draw_NewImage))
                {
                    g.DrawImage(img, 0, 0, height, width);
                    draw_NewImage.Save(fullPath + name + ext);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion


    }
}
