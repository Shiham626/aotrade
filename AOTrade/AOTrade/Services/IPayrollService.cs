﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AOTrade.Models.Entity;
using AOTrade.Models.ViewModel;
using Microsoft.AspNetCore.Http;

namespace AOTrade.Services
{
   public interface IPayrollService
    {
        #region Employee
        EmployeeVM GetEmployee(int id);
        List<EmployeeVM> GetEmployee();
        Task SaveEmployeeAsync(EmployeeVM model);
        Task DeleteEmployeeAsync(int id);
        #endregion

        #region Salary
        EmpSalaryVM GetEmpSalary(int id);
        List<EmpSalaryVM> GetEmpSalary();
        Task SaveEmpSalaryAsync(EmpSalaryVM model);
        Task DeleteEmpSalaryAsync(int id);
        #endregion


        #region Salary
        EmpLoanVM GetEmpLoan(int id);
        List<EmpLoanVM> GetEmpLoan();
        Task SaveEmpLoanAsync(EmpLoanVM model);
        Task DeleteEmpLoanAsync(int id);
        #endregion


        #region Image
        bool Comm_ImageFormatForStoreWithNameAndExt(string name, IFormFile file, string savePath, int height, int width, string ext);
        #endregion

    }
}
