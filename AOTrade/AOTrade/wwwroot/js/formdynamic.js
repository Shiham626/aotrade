﻿function FormateTableAA() {
    var dataAct = $('#tableDataUrlAA').val();
    $.ajax({
        url: dataAct,
        type: "Get",
        success: function (response) {
            $("#dataListAA").html(response);
            $('.data-table').DataTable();
        }
    });
}

function FormateTable() {
    var dataAct = $('#datatableUrl').val();
    $.ajax({
        url: dataAct,
        type: "Get",
        success: function (response) {
            $("#xdataList").html(response)
            $('#data-table').DataTable();
        }
    });
}

var lodedformdataAA = "";
var INDlodedformdataBB = "";


$(document).ready(function () {
    //----------------------------------------------------                  ------------------------------------------------
    //---------------------------------------------------- Common in dynamic -----------------------------------------------
    //----------------------------------------------------                  ------------------------------------------------

    ////////  ---------  Search Item by Item group-----------//////////

    $("#viewGenericAA").on("click", ".btnsearchItembtnAA", function () { 
        var groupchoosed = $('#itemGroupSelectAA').val();
        if (groupchoosed == "") { ToastNotification("error", "No group selected", "Error !"); }
        else {
            var formData = "id=" + groupchoosed;
            $.ajax({
                url: "/Common/GetItemList",
                type: "get",
                data: formData,
                success: function (response) {
                    if (response.act != false) {
                        $("#ietemNameAA").html(response);
                        $(".select2").select2();
                    }
                    else {
                        ToastNotification("error", "Data not found", "Error !");
                    }
                }
            });
        }        
    });

    $("#INDviewGenericBB").on("click", ".INDbtnsearchItembtnBB", function () {
        var groupchoosed = $('#INDitemGroupSelectBB').val();
        if (groupchoosed == "") { ToastNotification("error", "No group selected", "Error !"); }
        else {
            var formData = "id=" + groupchoosed;
            $.ajax({
                url: "/Common/GetItemList",
                type: "get",
                data: formData,
                success: function (response) {
                    if (response.act != false) {
                        $("#INDietemNameBB").html(response);
                        $(".select2").select2();
                    }
                    else {
                        ToastNotification("error", "Data not found", "Error !");
                    }
                }
            });
        }
    });
/////////---------------Common CRUD EXP---------//////////

    /////////form load
    //var formAct = $('#urlFormAA').val();
    //var now = new Date();
    //$.ajax({
    //    url: formAct,
    //    type: "Get",
    //    success: function (response) {
    //        lodedformdataAA = response;
    //        $("#formDataAA").html(lodedformdataAA);
    //        $(".select2").select2();
    //        $('.autonumber').autoNumeric('init');
    //    }
    //});
    //var formAct = $('#INDurlFormBB').val();
    //var now = new Date();
    //$.ajax({
    //    url: formAct,
    //    type: "Get",
    //    success: function (response) {
    //        INDlodedformdataBB = response;
    //        $("#INDformDataBB").html(INDlodedformdataBB);
    //        $(".select2").select2();
    //        $('.autonumber').autoNumeric('init');
    //    }
    //});


    ////table load
    //var dataAct = $('#tableDataUrlAA').val();
    //$.ajax({
    //    url: dataAct,
    //    type: "Get",
    //    success: function (response) {
    //        $("#dataListAA").html(response);
    //        $('.data-table').DataTable();
    //    }
    //});

    //var dataAct = $('#INDtableDataUrlBB').val();
    //$.ajax({
    //    url: dataAct,
    //    type: "Get",
    //    success: function (response) {
    //        $("#INDdataListBB").html(response);
    //        $('.data-table').DataTable();
    //    }
    //});


    //// Save model
    //$("#mainFormAA").submit(function (event) {
    //    //$('#elementsToOperateOn :input').attr('disabled', true);
    //    event.preventDefault(); //prevent default action
    //    var saveUrl = $(this).attr("action");
    //    var formData = new FormData(this);
    //    $.ajax({
    //        url: saveUrl,
    //        type: "post",
    //        processData: false,
    //        contentType: false,
    //        cache: false,
    //        data: formData,
    //        success: function (response) {
    //            if (response.act != false) {
    //                $("#formDataAA").html(lodedformdataAA);
    //                $(".select2").select2();
    //                $('.autonumber').autoNumeric('init');
    //                ToastNotification("success", "Changes saved successfully.", "Success !");
    //                $("#dataListAA").html(response);
    //                $('.data-table').DataTable();
    //            }
    //            else {
    //                ToastNotification("error", response.msg, "Error !");
    //            }
    //        }
    //    });
    //});

    //$("#INDmainFormBB").submit(function (event) {
    //    event.preventDefault(); //prevent default action
    //    var INDsaveUrl = $(this).attr("action");
    //    var INDformData = new FormData(this);
    //    $.ajax({
    //        url: INDsaveUrl,
    //        type: "post",
    //        processData: false,
    //        contentType: false,
    //        cache: false,
    //        data: INDformData,
    //        success: function (response) {
    //            if (response.act != false) {
    //                $("#INDformDataBB").html(INDlodedformdataBB);
    //                $(".select2").select2();
    //                $('.autonumber').autoNumeric('init');
    //                ToastNotification("success", "Changes saved successfully.", "Success !");
    //                $("#INDdataListBB").html(response);
    //                $('.data-table').DataTable();
    //            }
    //            else {
    //                ToastNotification("error", response.msg, "Error !");
    //            }
    //        }
    //    });
    //});
    //// Save model END


    //// Edit Foem Model
    //$("#viewGenericAA").on("click", ".add-update", function () {
    //    var editUrl = $('#urlFormAA').val();
    //    var url = editUrl; // the url to the controller
    //    var id = $(this).attr("id"); // the id that's given to each button in the list
    //    if (typeof id === "undefined") { id = 0; }

    //    $.get(url + "/" + id, function (response) {
    //        if (response.act == false) {
    //            ToastNotification("error", response.msg, "Oops !");
    //        }
    //        else {
    //            $("#formDataAA").html(response);
    //            $(".select2").select2();
    //            $('.autonumber').autoNumeric('init');
    //        }
    //    }).fail(function (request, exception, err) {
    //        ToastNotification("error", "There was an error!!!", "Error");
    //    });
    //});

    //$("#INDviewGenericBB").on("click", ".add-update", function () {
    //    var editUrl = $('#INDurlFormBB').val();
    //    var url = editUrl; // the url to the controller
    //    var id = $(this).attr("id"); // the id that's given to each button in the list
    //    if (typeof id === "undefined") { id = 0; }

    //    $.get(url + "/" + id, function (response) {
    //        if (response.act == false) {
    //            ToastNotification("error", response.msg, "Oops !");
    //        }
    //        else {
    //            $("#INDformDataBB").html(response);
    //            $(".select2").select2();
    //            $('.autonumber').autoNumeric('init');
    //        }
    //    }).fail(function (request, exception, err) {
    //        ToastNotification("error", "There was an error!!!", "Error");
    //    });
    //});



    ////Delete Model id
    //$("#viewGenericAA").on("click", ".delete", function () {
    //    var deleteUrl = $('#deleteUrlAA').val();
    //    DeleteWithSweetAlert($(this), deleteUrl);
    //});

    //$("#INDviewGenericBB").on("click", ".delete", function () {
    //    var deleteUrl = $('#deleteUrlAA').val();
    //    DeleteWithSweetAlert($(this), deleteUrl);
    //});
    ////////////////#######################///////////////##############  END common CRUD

    /////////-------------- Dynamic Start---------//////////

    // Add form row on click
    $("#genericView").on("click", "#add-form-row", function () {

                    var newRow = $("<tr>");
                    //var cols = response;
                    var cols = lodedformdata;
                    newRow.append(cols);

        $(".formUI").append(newRow);
                   // $("#formUI").html(response);
                    $(".select2").select2();

    });
    // Save model END

    ////////////////#######################///////////////##############  END Dynamic

    ///////////-----------///////////-----------approve
    $("#genericView").on("click", ".save-app", function () {
        var appUrl = "/Transection/ApproveRecords/";
        var dataToPost = "act=" + $('#apprAct').val()
        $.ajax({
            url: appUrl, // the url of the controller
            type: "post",
            data: dataToPost,
            success: function (response) {
                if (response.act === true) {
                    ToastNotification("success", response.msg, "Success");
                    window.swal("Saved !", "Record has been saved.", "success");
                    FormateTable();
                    //RefreshSideNav();
                }
                else {
                    ToastNotification("error", response.msg, "Error");
                }
            }
        })

    });


    //////---------////////--------- Admin edit later on , search by date to date
    $("#genericView").on("click", ".search-record", function (evt) {
        var dataAct = $('#datatableUrl').val();
        var datefrom = "from=" + $('#date-from').val();
        var dateto = "&to=" + $('#date-to').val();
        var formData = datefrom + dateto;
        $.ajax({
            url: dataAct,
            type: "get",
            data: formData,
            //contentType: false,
            //processData: false,
            success: function (response) {
                $("#xdataList").html(response)
                $('#data-table').DataTable();
            }
        });
    });


});










//////////////////--------------------//////////////////////-------------------///////////////////////-------------////////////////
// Toaste notification function
function ToastNotification(type, msg, title) {
    $.toast({
        heading: title,
        text: msg,
        position: 'top-right',
        icon: type,
        hideAfter: 2500,
        stack: 1
    });
}

// Save Confirm
function Confirm(type, msg, title) {
    swal(
        {
            title: title,
            text: msg,
            type: type,
            confirmButtonClass: 'btn btn-confirm mt-2'
        }
    )
}

//Delete wirh Parameter
function DeleteWithSweetAlert(obj, url) {
    swal({
        title: 'Are you sure?',
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success mt-2',
        cancelButtonClass: 'btn btn-danger ml-2 mt-2',
        buttonsStyling: false
    }).then(function () {

        var dataToPost = "id=" + obj.attr("id");
        var tr = obj.closest("tr");
        $.ajax({
            url: url, // the url of the controller
            type: "post",
            data: dataToPost,
            success: function (response) {
                if (response.act === true) {
                    swal({
                        title: 'Deleted !',
                        text: "Selected item has been deleted",
                        type: 'success',
                        confirmButtonClass: 'btn btn-confirm mt-2'
                    })
                    tr.remove();
                }
                else {
                    swal({
                        title: 'Error !',
                        text: "",
                        type: 'error',
                        confirmButtonClass: 'btn btn-confirm mt-2'
                    }
                    )
                }
            }
        })

        }, function (dismiss) {
            // dismiss can be 'cancel', 'overlay',
            // 'close', and 'timer'
            if (dismiss === 'cancel') {
                swal({
                    title: 'Cancelled',
                    text: "",
                    type: 'error',
                    confirmButtonClass: 'btn btn-confirm mt-2'
                }
                )
            }
        })
}


$(document).ready(function () {
    var counter = 0;

    $("#addrow").on("click", function () {
        var newRow = $("<tr>");
        var cols = "";

        cols += '<td><input type="text" class="form-control" name="name' + counter + '"/></td>';
        cols += '<td><input type="text" class="form-control" name="mail' + counter + '"/></td>';
        cols += '<td><input type="text" class="form-control" name="phone' + counter + '"/></td>';

        cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td>';
        newRow.append(cols);
        $("table.order-list").append(newRow);
        counter++;
    });



    $("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();
        counter -= 1
    });


});



function calculateRow(row) {
    var price = +row.find('input[name^="price"]').val();

}

function calculateGrandTotal() {
    var grandTotal = 0;
    $("table.order-list").find('input[name^="price"]').each(function () {
        grandTotal += +$(this).val();
    });
    $("#grandtotal").text(grandTotal.toFixed(2));
}