﻿

var lodedformdata = "";


$(document).ready(function () {

    //----------------------------------------------------                  ------------------------------------------------
    //---------------------------------------------------- Common in project -----------------------------------------------
    //----------------------------------------------------                  ------------------------------------------------
////////  ---------  Search Item by Item group-----------//////////

    /////--------------  Select 2 EVENT  ---------///////////
    $("#xformdata").on("click", '#groupSelect', function () {
        $('#groupSelect').on('select2:select', function (grp) {
            var groupchoosed = grp.params.data.id;
            if (groupchoosed == "") { ToastNotification("error", "No Group selected", "Error !"); }
            else {
                var formData = "id=" + groupchoosed;
                $.ajax({
                    url: "/Common/GetItemList",
                    type: "get",
                    data: formData,
                    success: function (response) {
                        if (response.act != false) {
                            $("#ietemNameSelect").html(response);
                            $(".select2").select2();
                        }
                        else {
                            ToastNotification("error", "Data not found", "Error !");
                        }
                    }
                });
            }
        });
    });

    $("#xformdata").on("click", '#ietemNameSelect', function () {
        $('#ietemNameSelect').on('select2:select', function (itm) {
            var Itemchoosed = itm.params.data.id;
            if (Itemchoosed == "") { ToastNotification("error", "No Item selected", "Error !"); }
            else {
                var ItemformData = "id=" + Itemchoosed;
                $.ajax({
                    url: "/Common/GetItemDescription",
                    type: "get",
                    data: ItemformData,
                    success: function (response) {
                        if (response.act != false) {
                            $("#item-desp").html(response);
                        }
                        else {
                            ToastNotification("error", "Data not found", "Error !");
                        }
                    }
                });
            }
        });
    });
    /////-------------- E N D  Select 2 EVENT  ---------///////////

/////////---------------Common CRUD---------//////////

    /////////form load
    var formAct = $('#formUrl').val();
    var now = new Date();
    $.ajax({
        url: formAct,
        type: "Get",
        success: function (response) {
            lodedformdata = response;
            $("#formUI").html(lodedformdata);
            $(".select2").select2();
            $('.autonumber').autoNumeric('init');
        }
    });


    //table load
    var dataAct = $('#datatableUrl').val();
    $.ajax({
        url: dataAct,
        type: "Get",
        success: function (response) {
            $("#xdataList").html(response);
            $('.data-table').DataTable();
        }
    });


    // Edit Form Model
    $("#genericView").on("click", ".add-update", function () {
        var editUrl = $('#formUrl').val();
        var url = editUrl; // the url to the controller
        var id = $(this).attr("id"); // the id that's given to each button in the list
        if (typeof id === "undefined") { id = 0; }

        $.get(url + "/" + id, function (response) {
            if (response.act == false) {
                ToastNotification("error", response.msg, "Oops !");
            }
            else {
                $("#formUI").html(response);
                $(".select2").select2();
                $('.autonumber').autoNumeric('init');
            }
        }).fail(function (request, exception, err) {
            ToastNotification("error", "There was an error!!!", "Error");
        });
    });

    // Save model
    $("#xformdata").submit(function (event) {
        event.preventDefault(); //prevent default action
        var dated = $('#dated').val();
        var saveUrl = $(this).attr("action");
        var formData = new FormData(this);
        $.ajax({
            url: saveUrl,
            type: "post",
            processData: false,
            contentType: false,
            async: false,
            cache: false,
            data: formData,
            success: function (response) {
                if (response.act != false) {
                    $("#formUI").html(lodedformdata);
                    $("#dated").val(dated);
                    $('.autonumber').autoNumeric('init');
                    $(".select2").select2();
                    ToastNotification("success", "Changes saved successfully.", "Success !");
                    $("#xdataList").html(response)
                    $('.data-table').DataTable();
                }
                else {
                    ToastNotification("error", response.msg, "Error !");
                }
            }
        });
    });
    // Save model END

    //Delete Model id
    $("#genericView").on("click", ".delete", function () {
        var deleteUrl = $('#deleteUrl').val();
        DeleteWithSweetAlert($(this), deleteUrl);
    });
    ////////////////#######################///////////////##############  END common CRUD

    /////////-------------- Dynamic Start---------//////////

    // Add form row on click
    $("#genericView").on("click", "#add-form-row", function () {

                    var newRow = $("<tr>");
                    //var cols = response;
                    var cols = lodedformdata;
                    newRow.append(cols);

        $(".formUI").append(newRow);
                   // $("#formUI").html(response);
                    $(".select2").select2();

    });
    // Save model END

    ////////////////#######################///////////////##############  END Dynamic

    ///////////-----------///////////-----------approve
    $("#genericView").on("click", ".save-app", function () {
        var appUrl = "/Transection/ApproveRecords/";
        var dataToPost = "act=" + $('#apprAct').val()
        $.ajax({
            url: appUrl, // the url of the controller
            type: "post",
            data: dataToPost,
            success: function (response) {
                if (response.act === true) {
                    ToastNotification("success", response.msg, "Success");
                    window.swal("Saved !", "Record has been saved.", "success");
                    //FormateTable();
                    //RefreshSideNav();
                }
                else {
                    ToastNotification("error", response.msg, "Error");
                }
            }
        })

    });


    //////---------////////--------- Admin edit later on , search by date to date
    $("#genericView").on("click", ".search-record", function (evt) {
        var dataAct = $('#datatableUrl').val();
        var datefrom = "from=" + $('#date-from').val();
        var dateto = "&to=" + $('#date-to').val();
        var formData = datefrom + dateto;
        $.ajax({
            url: dataAct,
            type: "get",
            data: formData,
            //contentType: false,
            //processData: false,
            success: function (response) {
                $("#xdataList").html(response)
                $('#data-table').DataTable();
            }
        });
    });



    // BONUS Report

    $("#reportsData").submit(function (event) {
        event.preventDefault(); //prevent default action
        var saveUrl = $(this).attr("action");
        var formData = new FormData(this);
        $.ajax({
            url: saveUrl,
            type: "post",
            processData: false,
            contentType: false,
            async: false,
            cache: false,
            data: formData,
            success: function (response) {
                if (response.act != false) {
                    $("#reportsList").html(response)
                }
                else {
                    ToastNotification("error", "Data not found", "Error !");
                }
            }
        });
    });


});










//////////////////--------------------//////////////////////-------------------///////////////////////-------------////////////////
// Toaste notification function
function ToastNotification(type, msg, title) {
    $.toast({
        heading: title,
        text: msg,
        position: 'top-right',
        icon: type,
        hideAfter: 2500,
        stack: 1
    });
}

// Save Confirm
function Confirm(type, msg, title) {
    swal(
        {
            title: title,
            text: msg,
            type: type,
            confirmButtonClass: 'btn btn-confirm mt-2'
        }
    )
}

//Delete wirh Parameter
function DeleteWithSweetAlert(obj, url) {
    swal({
        title: 'Are you sure?',
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success mt-2',
        cancelButtonClass: 'btn btn-danger ml-2 mt-2',
        buttonsStyling: false
    }).then(function () {

        var dataToPost = "id=" + obj.attr("id");
        var tr = obj.closest("tr");
        $.ajax({
            url: url, // the url of the controller
            type: "post",
            data: dataToPost,
            success: function (response) {
                if (response.act === true) {
                    swal({
                        title: 'Deleted !',
                        text: "Selected item has been deleted",
                        type: 'success',
                        confirmButtonClass: 'btn btn-confirm mt-2'
                    })
                    tr.remove();
                }
                else {
                    swal({
                        title: 'Error !',
                        text: "",
                        type: 'error',
                        confirmButtonClass: 'btn btn-confirm mt-2'
                    }
                    )
                }
            }
        })

        }, function (dismiss) {
            // dismiss can be 'cancel', 'overlay',
            // 'close', and 'timer'
            if (dismiss === 'cancel') {
                swal({
                    title: 'Cancelled',
                    text: "",
                    type: 'error',
                    confirmButtonClass: 'btn btn-confirm mt-2'
                }
                )
            }
        })
}