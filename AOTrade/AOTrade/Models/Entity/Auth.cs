﻿using System;
using System.Collections.Generic;

namespace AOTrade.Models.Entity
{
    public partial class Auth
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string Remarks { get; set; }
        public int? CreateUser { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
