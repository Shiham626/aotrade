﻿using System;
using System.Collections.Generic;

namespace AOTrade.Models.Entity
{
    public partial class ItemGroup
    {
        public ItemGroup()
        {
            Expense = new HashSet<Expense>();
            Income = new HashSet<Income>();
            Item = new HashSet<Item>();
        }

        public int Id { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }
        public bool Editable { get; set; }
        public string Type { get; set; }
        public int? CreateUser { get; set; }
        public DateTime? CreateDate { get; set; }

        public ICollection<Expense> Expense { get; set; }
        public ICollection<Income> Income { get; set; }
        public ICollection<Item> Item { get; set; }
    }
}
