﻿using System;
using System.Collections.Generic;

namespace AOTrade.Models.Entity
{
    public partial class EmpLoan
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public decimal LoanAmmount { get; set; }
        public decimal MonthlyDeduct { get; set; }
        public DateTime DeductDate { get; set; }
        public decimal DueAmmount { get; set; }
        public DateTime? Date { get; set; }
        public string Remarks { get; set; }
        public int? CreateUser { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ApproveDate { get; set; }

        public Employee Emp { get; set; }
    }
}
