﻿using System;
using System.Collections.Generic;

namespace AOTrade.Models.Entity
{
    public partial class Item
    {
        public Item()
        {
            Expense = new HashSet<Expense>();
            Income = new HashSet<Income>();
        }

        public int Id { get; set; }
        public int GroupId { get; set; }
        public string ItemName { get; set; }
        public int? OuterTableId { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public int? CreateUser { get; set; }
        public DateTime? CreateDate { get; set; }

        public ItemGroup Group { get; set; }
        public ICollection<Expense> Expense { get; set; }
        public ICollection<Income> Income { get; set; }
    }
}
