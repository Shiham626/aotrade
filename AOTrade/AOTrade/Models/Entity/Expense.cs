﻿using System;
using System.Collections.Generic;

namespace AOTrade.Models.Entity
{
    public partial class Expense
    {
        public int Id { get; set; }
        public int ItemGroupId { get; set; }
        public int ItemId { get; set; }
        public string ExpenseType { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public int? CreateUser { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ApproveDate { get; set; }

        public Item Item { get; set; }
        public ItemGroup ItemGroup { get; set; }
    }
}
