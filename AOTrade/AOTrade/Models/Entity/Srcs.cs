﻿using System;
using System.Collections.Generic;

namespace AOTrade.Models.Entity
{
    public partial class Srcs
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string CodeWord { get; set; }
        public decimal? Crr { get; set; }
    }
}
