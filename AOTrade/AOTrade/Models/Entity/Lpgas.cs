﻿using System;
using System.Collections.Generic;

namespace AOTrade.Models.Entity
{
    public partial class Lpgas
    {
        public Lpgas()
        {
            Lpcatagory = new HashSet<Lpcatagory>();
            LpstockCredit = new HashSet<LpstockCredit>();
            LpstockDebit = new HashSet<LpstockDebit>();
        }

        public int Id { get; set; }
        public string LpbrandName { get; set; }
        public string Description { get; set; }
        public int? CreateUser { get; set; }
        public DateTime? CreateDate { get; set; }

        public ICollection<Lpcatagory> Lpcatagory { get; set; }
        public ICollection<LpstockCredit> LpstockCredit { get; set; }
        public ICollection<LpstockDebit> LpstockDebit { get; set; }
    }
}
