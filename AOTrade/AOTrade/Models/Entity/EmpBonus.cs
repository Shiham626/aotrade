﻿using System;
using System.Collections.Generic;

namespace AOTrade.Models.Entity
{
    public partial class EmpBonus
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public decimal Salary { get; set; }
        public int BonusPercent { get; set; }
        public decimal BonusAmmount { get; set; }
        public string Remarks { get; set; }
        public DateTime Date { get; set; }
        public int? CreateUser { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ApproveDate { get; set; }

        public Employee Emp { get; set; }
    }
}
