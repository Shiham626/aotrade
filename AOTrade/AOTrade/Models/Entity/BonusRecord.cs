﻿using System;
using System.Collections.Generic;

namespace AOTrade.Models.Entity
{
    public partial class BonusRecord
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Record { get; set; }
        public decimal? BonusPercent { get; set; }
        public decimal? TotalPay { get; set; }
        public DateTime? Date { get; set; }
        public int? CreateUser { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ApproveDate { get; set; }
    }
}
