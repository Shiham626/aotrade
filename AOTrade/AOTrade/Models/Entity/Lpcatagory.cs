﻿using System;
using System.Collections.Generic;

namespace AOTrade.Models.Entity
{
    public partial class Lpcatagory
    {
        public Lpcatagory()
        {
            LpstockCredit = new HashSet<LpstockCredit>();
            LpstockDebit = new HashSet<LpstockDebit>();
        }

        public int Id { get; set; }
        public int LpGasId { get; set; }
        public string CatagoryName { get; set; }
        public int Quantuty { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal ItemPrice { get; set; }
        public int? CreateUser { get; set; }
        public DateTime? CreateDate { get; set; }

        public Lpgas LpGas { get; set; }
        public ICollection<LpstockCredit> LpstockCredit { get; set; }
        public ICollection<LpstockDebit> LpstockDebit { get; set; }
    }
}
