﻿using System;
using System.Collections.Generic;

namespace AOTrade.Models.Entity
{
    public partial class EmpIncrement
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public decimal PreviousSalary { get; set; }
        public decimal Increment { get; set; }
        public decimal GrossAmmount { get; set; }
        public DateTime? Date { get; set; }
        public string Remarks { get; set; }
        public int? CreateUser { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ApproveDate { get; set; }

        public Employee Emp { get; set; }
    }
}
