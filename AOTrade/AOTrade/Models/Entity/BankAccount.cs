﻿using System;
using System.Collections.Generic;

namespace AOTrade.Models.Entity
{
    public partial class BankAccount
    {
        public int Id { get; set; }
        public string BankName { get; set; }
        public string AccountNo { get; set; }
        public string Desctiption { get; set; }
        public decimal? CurrentAmmount { get; set; }
        public int? CreateUser { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
