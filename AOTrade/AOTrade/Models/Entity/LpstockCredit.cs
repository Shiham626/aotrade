﻿using System;
using System.Collections.Generic;

namespace AOTrade.Models.Entity
{
    public partial class LpstockCredit
    {
        public int Id { get; set; }
        public int LpGasId { get; set; }
        public string Source { get; set; }
        public int CatagoryId { get; set; }
        public int Quantity { get; set; }
        public decimal TotalAmmount { get; set; }
        public DateTime Date { get; set; }
        public int? CreateUser { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ApproveDate { get; set; }

        public Lpcatagory Catagory { get; set; }
        public Lpgas LpGas { get; set; }
    }
}
