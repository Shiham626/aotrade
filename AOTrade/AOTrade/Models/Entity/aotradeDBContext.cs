﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AOTrade.Models.Entity
{
    public partial class aotradeDBContext : DbContext
    {
        public aotradeDBContext(DbContextOptions<aotradeDBContext> options)
               : base(options)
        {
        }
        public virtual DbSet<Auth> Auth { get; set; }
        public virtual DbSet<BankAccount> BankAccount { get; set; }
        public virtual DbSet<BonusRecord> BonusRecord { get; set; }
        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<EmpBonus> EmpBonus { get; set; }
        public virtual DbSet<EmpIncrement> EmpIncrement { get; set; }
        public virtual DbSet<EmpLoan> EmpLoan { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<EmpSalary> EmpSalary { get; set; }
        public virtual DbSet<Expense> Expense { get; set; }
        public virtual DbSet<Income> Income { get; set; }
        public virtual DbSet<Item> Item { get; set; }
        public virtual DbSet<ItemGroup> ItemGroup { get; set; }
        public virtual DbSet<Lpcatagory> Lpcatagory { get; set; }
        public virtual DbSet<Lpgas> Lpgas { get; set; }
        public virtual DbSet<LpstockCredit> LpstockCredit { get; set; }
        public virtual DbSet<LpstockDebit> LpstockDebit { get; set; }
        public virtual DbSet<Srcs> Srcs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Auth>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Email)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Remarks).HasColumnType("text");

                entity.Property(e => e.Role)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<BankAccount>(entity =>
            {
                entity.Property(e => e.AccountNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BankName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Desctiption).IsUnicode(false);
            });

            modelBuilder.Entity<BonusRecord>(entity =>
            {
                entity.Property(e => e.ApproveDate).HasColumnType("date");

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.Record).IsUnicode(false);

                entity.Property(e => e.Title).IsUnicode(false);
            });

            modelBuilder.Entity<Company>(entity =>
            {
                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Description).IsUnicode(false);
            });

            modelBuilder.Entity<EmpBonus>(entity =>
            {
                entity.Property(e => e.ApproveDate).HasColumnType("date");

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Remarks).IsUnicode(false);

                entity.HasOne(d => d.Emp)
                    .WithMany(p => p.EmpBonus)
                    .HasForeignKey(d => d.EmpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EmpBonus_Employee");
            });

            modelBuilder.Entity<EmpIncrement>(entity =>
            {
                entity.Property(e => e.ApproveDate).HasColumnType("date");

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Remarks).IsUnicode(false);

                entity.HasOne(d => d.Emp)
                    .WithMany(p => p.EmpIncrement)
                    .HasForeignKey(d => d.EmpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EmpIncrement_Employee");
            });

            modelBuilder.Entity<EmpLoan>(entity =>
            {
                entity.Property(e => e.ApproveDate).HasColumnType("date");

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.DeductDate).HasColumnType("date");

                entity.Property(e => e.Remarks).IsUnicode(false);

                entity.HasOne(d => d.Emp)
                    .WithMany(p => p.EmpLoan)
                    .HasForeignKey(d => d.EmpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EmpLoan_Employee");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Address).IsUnicode(false);

                entity.Property(e => e.BirthDate).HasColumnType("date");

                entity.Property(e => e.BloodGroup)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Designation)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmpName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmpNameId)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.FatherName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.JoinDate).HasColumnType("date");

                entity.Property(e => e.MotherName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NidNo)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNo)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Photo)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EmpSalary>(entity =>
            {
                entity.Property(e => e.ApproveDate).HasColumnType("date");

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.HasOne(d => d.Emp)
                    .WithMany(p => p.EmpSalary)
                    .HasForeignKey(d => d.EmpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EmpSalary_Employee");
            });

            modelBuilder.Entity<Expense>(entity =>
            {
                entity.Property(e => e.ApproveDate).HasColumnType("date");

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Description)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ExpenseType)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.ItemGroup)
                    .WithMany(p => p.Expense)
                    .HasForeignKey(d => d.ItemGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Expense_ItemGroup");

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.Expense)
                    .HasForeignKey(d => d.ItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Expense_Item");
            });

            modelBuilder.Entity<Income>(entity =>
            {
                entity.Property(e => e.ApproveDate).HasColumnType("date");

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Description)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.ItemGroup)
                    .WithMany(p => p.Income)
                    .HasForeignKey(d => d.ItemGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Income_ItemGroup");

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.Income)
                    .HasForeignKey(d => d.ItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Income_Item");
            });

            modelBuilder.Entity<Item>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Description)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ItemName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.Item)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Item_ItemGroup");
            });

            modelBuilder.Entity<ItemGroup>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Description)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.GroupName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Lpcatagory>(entity =>
            {
                entity.ToTable("LPCatagory");

                entity.Property(e => e.CatagoryName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.HasOne(d => d.LpGas)
                    .WithMany(p => p.Lpcatagory)
                    .HasForeignKey(d => d.LpGasId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LPCatagory_LPGas");
            });

            modelBuilder.Entity<Lpgas>(entity =>
            {
                entity.ToTable("LPGas");

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.LpbrandName)
                    .IsRequired()
                    .HasColumnName("LPBrandName")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LpstockCredit>(entity =>
            {
                entity.ToTable("LPStockCredit");

                entity.Property(e => e.ApproveDate).HasColumnType("date");

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Source)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.Catagory)
                    .WithMany(p => p.LpstockCredit)
                    .HasForeignKey(d => d.CatagoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LPStockCredit_LPCatagory");

                entity.HasOne(d => d.LpGas)
                    .WithMany(p => p.LpstockCredit)
                    .HasForeignKey(d => d.LpGasId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LPStockCredit_LPGas");
            });

            modelBuilder.Entity<LpstockDebit>(entity =>
            {
                entity.ToTable("LPStockDebit");

                entity.Property(e => e.ApproveDate).HasColumnType("date");

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Destination)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.Catagory)
                    .WithMany(p => p.LpstockDebit)
                    .HasForeignKey(d => d.CatagoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LPStockDebit_LPCatagory");

                entity.HasOne(d => d.LpGas)
                    .WithMany(p => p.LpstockDebit)
                    .HasForeignKey(d => d.LpGasId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LPStockDebit_LPGas");
            });

            modelBuilder.Entity<Srcs>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CodeWord)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Crr).HasColumnName("CRR");
            });
        }
    }
}
