﻿using System;
using System.Collections.Generic;

namespace AOTrade.Models.Entity
{
    public partial class Company
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public string Description { get; set; }
        public decimal CompanyPercent { get; set; }
        public decimal? CurrentAmmount { get; set; }
        public decimal? CurrentStockAmmount { get; set; }
        public int? CreateUser { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
