﻿using System;
using System.Collections.Generic;

namespace AOTrade.Models.Entity
{
    public partial class Employee
    {
        public Employee()
        {
            EmpBonus = new HashSet<EmpBonus>();
            EmpIncrement = new HashSet<EmpIncrement>();
            EmpLoan = new HashSet<EmpLoan>();
            EmpSalary = new HashSet<EmpSalary>();
        }

        public int Id { get; set; }
        public string EmpName { get; set; }
        public string EmpNameId { get; set; }
        public string Photo { get; set; }
        public string Designation { get; set; }
        public DateTime? JoinDate { get; set; }
        public string NidNo { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public string Address { get; set; }
        public DateTime? BirthDate { get; set; }
        public string BloodGroup { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public bool? IsEmployeeActive { get; set; }
        public decimal Salary { get; set; }
        public decimal BeginSalary { get; set; }
        public int? LoanId { get; set; }
        public decimal LoanAmmount { get; set; }
        public int? CreateUser { get; set; }
        public DateTime? CreateDate { get; set; }

        public ICollection<EmpBonus> EmpBonus { get; set; }
        public ICollection<EmpIncrement> EmpIncrement { get; set; }
        public ICollection<EmpLoan> EmpLoan { get; set; }
        public ICollection<EmpSalary> EmpSalary { get; set; }
    }
}
