﻿using System;
using System.Collections.Generic;

namespace AOTrade.Models.Entity
{
    public partial class EmpSalary
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public int? SalaryMonth { get; set; }
        public int? SalaryYear { get; set; }
        public decimal LoanDueAmmount { get; set; }
        public decimal LoanDeduct { get; set; }
        public int AbsentDay { get; set; }
        public decimal AbsentDeduct { get; set; }
        public decimal Deduction { get; set; }
        public decimal GrossSalary { get; set; }
        public DateTime Date { get; set; }
        public int? CreateUser { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ApproveDate { get; set; }

        public Employee Emp { get; set; }
    }
}
