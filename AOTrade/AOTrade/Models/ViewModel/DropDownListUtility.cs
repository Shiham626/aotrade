﻿
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace AOTrade.Models.ViewModel
{

    public static class DropDownListUtility
    {
        public static IEnumerable<SelectListItem> GetMonthDropdown(object selectedValue)
        {
            return new List<SelectListItem>
            {
                new SelectListItem{ Text="January", Value = "January", Selected = "January" == selectedValue.ToString()},
                new SelectListItem{ Text="February", Value = "February", Selected = "February" == selectedValue.ToString()},
                new SelectListItem{ Text="March", Value = "March", Selected = "March" == selectedValue.ToString()},
                new SelectListItem{ Text="April", Value = "April", Selected = "April" == selectedValue.ToString()},
                new SelectListItem{ Text="May", Value = "May", Selected = "May" == selectedValue.ToString()},
                new SelectListItem{ Text="June", Value = "June", Selected = "June" == selectedValue.ToString()},
                new SelectListItem{ Text="July", Value = "July", Selected = "July" == selectedValue.ToString()},
                new SelectListItem{ Text="August", Value = "August", Selected = "August" == selectedValue.ToString()},
                new SelectListItem{ Text="September", Value = "September", Selected = "September" == selectedValue.ToString()},
                new SelectListItem{ Text="October", Value = "October", Selected = "October" == selectedValue.ToString()},
                new SelectListItem{ Text="November", Value = "November", Selected = "November" == selectedValue.ToString()},
                new SelectListItem{ Text="December", Value = "December", Selected = "December" == selectedValue.ToString()},
            };
        }

        public static IEnumerable<SelectListItem> GetFreeItemNameDropdown(object selectedValue)
        {
            return new List<SelectListItem>
            {
                new SelectListItem{ Text="Expire Product", Value = "Expire Product", Selected = "Expire Product" == selectedValue.ToString()},
                new SelectListItem{ Text="Free Item - Commission", Value = "Free Item - Commission", Selected = "Free Item - Commission " == selectedValue.ToString()},
                new SelectListItem{ Text="Free Item - Free sales", Value = "Free Item - Free sales", Selected = "Free Item - Free sales " == selectedValue.ToString()},
                new SelectListItem{ Text="Free Item - Promotion", Value = "Free Item - Promotion", Selected = "Free Item - Promotion " == selectedValue.ToString()},
            };
        }

        public static IEnumerable<SelectListItem> GetUserRoleDropdown(object selectedValue)
        {
            return new List<SelectListItem>
            {
                new SelectListItem{ Text="Admin", Value = "Admin", Selected = "Admin" == selectedValue.ToString()},
                new SelectListItem{ Text="Supervisor", Value = "Supervisor", Selected = "Supervisor" == selectedValue.ToString()},
                new SelectListItem{ Text="Operator", Value = "Operator", Selected = "Operator" == selectedValue.ToString()},
            };
        }

        public static IEnumerable<SelectListItem> GetCashTypeDropdown(object selectedValue)
        {
            return new List<SelectListItem>
            {
                new SelectListItem{ Text="Direct", Value = "Direct", Selected = "Direct" == selectedValue.ToString()},
                new SelectListItem{ Text="Indirect", Value = "Indirect", Selected = "Indirect" == selectedValue.ToString()},
            };
        }

        public static IEnumerable<SelectListItem> GetLpCreditSourceDropdown(object selectedValue)
        {
            return new List<SelectListItem>
            {
                new SelectListItem{ Text="Brand", Value = "Brand", Selected = "Brand" == selectedValue.ToString()},
                new SelectListItem{ Text="Depo", Value = "Depo", Selected = "Depo" == selectedValue.ToString()},
                new SelectListItem{ Text="Retailer", Value = "Retailer", Selected = "Retailer" == selectedValue.ToString()},
            };
        }

        public static IEnumerable<SelectListItem> GetLpDebtDestinationDropdown(object selectedValue)
        {
            return new List<SelectListItem>
            {
                new SelectListItem{ Text="Depo", Value = "Depo", Selected = "Depo" == selectedValue.ToString()},
                new SelectListItem{ Text="Retailer", Value = "Retailer", Selected = "Retailer" == selectedValue.ToString()},
            };
        }
    }
}