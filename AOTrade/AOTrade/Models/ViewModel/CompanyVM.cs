﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AOTrade.Models.ViewModel
{
    public class CompanyVM
    {
        public int Id { get; set; }

        [Required]
        public string CompanyName { get; set; }
        public string Description { get; set; }

        [Required]
        [Range(0.01, 999, ErrorMessage = "Must be greater then 0")]
        public decimal? CompanyPercent { get; set; }

        public decimal? CurrentAmmount { get; set; }
        public decimal? CurrentStockAmmount { get; set; }
        public int? CreateUser { get; set; }

        [Display(Name = "Create date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CreateDate { get; set; }
    }
}
