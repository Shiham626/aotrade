﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AOTrade.Models.Entity;

namespace AOTrade.Models.ViewModel
{
    public class EmpSalaryVM
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public int? SalaryMonth { get; set; }
        public int? SalaryYear { get; set; }
        public decimal LoanDueAmmount { get; set; }
        public decimal LoanDeduct { get; set; }
        public int AbsentDay { get; set; }

        [Range(1, 31, ErrorMessage = "Must be in between 0 to 31")]
        public decimal AbsentDeduct { get; set; }
        public decimal Deduction { get; set; }
        public decimal GrossSalary { get; set; }

        [Display(Name = "Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        public int? CreateUser { get; set; }

        [Display(Name = "Create date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CreateDate { get; set; }

        [Display(Name = "Approve date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? ApproveDate { get; set; }

        public Employee Emp { get; set; }

        public List<EmployeeVM> EmployeeVMList { get; set; }

        public List<EmpSalaryVM> EmpSalaryVMList { get; set; }
    }
}
