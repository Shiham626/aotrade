﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AOTrade.Models.Entity;

namespace AOTrade.Models.ViewModel
{
    public partial class EmpLoanVM
    {
        public int Id { get; set; }
        public int EmpId { get; set; }

        [Range(0.01, 999999999999, ErrorMessage = "Must be greater then 0")]
        public decimal? LoanAmmount { get; set; }

        [Range(0.01, 999999999999, ErrorMessage = "Must be greater then 0")]
        public decimal? MonthlyDeduct { get; set; }


        [Display(Name = "Deduct date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DeductDate { get; set; }

        [Range(0.01, 999999999999, ErrorMessage = "Must be greater then 0")]
        public decimal? DueAmmount { get; set; }

        public string Remarks { get; set; }
        public int? CreateUser { get; set; }

        [Display(Name = "Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Date { get; set; }


        [Display(Name = "Create date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CreateDate { get; set; }

        [Display(Name = "Approve date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? ApproveDate { get; set; }

        public Employee Emp { get; set; }
    }
}
