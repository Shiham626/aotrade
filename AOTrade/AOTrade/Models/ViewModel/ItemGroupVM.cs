﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AOTrade.Models.ViewModel
{
    public class ItemGroupVM
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Group name")]
        public string GroupName { get; set; }

        public string Description { get; set; }

        public bool Editable { get; set; }
        public bool? ForIncome { get; set; }
        public bool? ForDirectExp { get; set; }
        public bool? ForIndirectExp { get; set; }

        public string Type { get; set; }
        public int? CreateUser { get; set; }

        [Display(Name = "Create date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CreateDate { get; set; }

    }
}
