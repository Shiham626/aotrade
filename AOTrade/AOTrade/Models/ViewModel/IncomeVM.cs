﻿using AOTrade.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AOTrade.Models.ViewModel
{
    public class IncomeVM
    {
        public int Id { get; set; }
        public int ItemGroupId { get; set; }
        public int ItemId { get; set; }

        [Range(0.01, 999999999999, ErrorMessage = "Must be greater then 0")]
        [DisplayFormat(DataFormatString = "{0:0,0.00}")]
        public decimal? Amount { get; set; }

        public string Description { get; set; }

        [Display(Name = "Create date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        public int? CreateUser { get; set; }

        [Display(Name = "Create date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CreateDate { get; set; }

        [Display(Name = "Create date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? ApproveDate { get; set; }

        public Item Item { get; set; }
        public ItemGroup ItemGroup { get; set; }
    }
}
