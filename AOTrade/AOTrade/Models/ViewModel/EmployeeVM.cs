﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AOTrade.Models.ViewModel
{
    public class EmployeeVM
    {
        public int Id { get; set; }
        public string EmpName { get; set; }
        public string Photo { get; set; }
        public string Designation { get; set; }

        [Display(Name = "Create date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM/dd/yyyy}", ApplyFormatInEditMode = true)]

        public string EmpNameId { get; set; }

        public DateTime? JoinDate { get; set; }

        public string NidNo { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public string Address { get; set; }

        [Display(Name = "Create date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? BirthDate { get; set; }
        public string BloodGroup { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public bool? IsEmployeeActive { get; set; }
        public decimal? Salary { get; set; }
        public decimal BeginSalary { get; set; }
        public int? LoanId { get; set; }
        public decimal LoanAmmount { get; set; }
        public int? CreateUser { get; set; }

        [Display(Name = "Create date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CreateDate { get; set; }

        // Extra value

        public int empNewId;
    }
}
