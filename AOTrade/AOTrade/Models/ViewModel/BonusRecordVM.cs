﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AOTrade.Models.ViewModel
{
    public class BonusRecordVM
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Record { get; set; }

        [Range(0.01, 999, ErrorMessage = "Must be greater then 0")]
        public decimal? BonusPercent { get; set; }

        [Range(0.01, 999999999999, ErrorMessage = "Must be greater then 0")]
        public decimal? TotalPay { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Date { get; set; }
        public int? CreateUser { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ApproveDate { get; set; }

        public List<EmployeeVM> EmployeeVMList { get; set; }
    }
}
