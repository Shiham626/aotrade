﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AOTrade.Models.Entity;

namespace AOTrade.Models.ViewModel
{
    public class EmpBonusVM
    {
        public int Id { get; set; }
        public int EmpId { get; set; }

        [Range(0.01, 999999999999, ErrorMessage = "Must be greater then 0")]
        public decimal Salary { get; set; }

        [Range(0.01, 9999, ErrorMessage = "Must be greater then 0")]
        public int? BonusPercent { get; set; }

        [Range(0.01, 999999999999, ErrorMessage = "Must be greater then 0")]
        public decimal BonusAmmount { get; set; }

        public string Remarks { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        public int? CreateUser { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CreateDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? ApproveDate { get; set; }

        public Employee Emp { get; set; }


        public List<EmployeeVM> EmployeeVMList { get; set; }
    }
}
