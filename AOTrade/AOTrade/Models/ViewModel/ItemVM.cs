﻿using AOTrade.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AOTrade.Models.ViewModel
{
    public class ItemVM
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Item group")]
        public int GroupId { get; set; }

        [Required]
        [Display(Name = "Item name")]
        public string ItemName { get; set; }

        public int? OuterTableId { get; set; }

        public string Description { get; set; }

        public string Type { get; set; }
        public int? CreateUser { get; set; }

        [Display(Name = "Create date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CreateDate { get; set; }

        public ItemGroup Group { get; set; }
    }
}
