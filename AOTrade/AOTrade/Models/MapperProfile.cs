﻿using AOTrade.Models.Entity;
using AOTrade.Models.ViewModel;
using AutoMapper;

namespace AOTrade.Models
{
    public class MapperProfile: Profile 
    {
        public MapperProfile()
        {
            CreateMap<Item, ItemVM>()
                    .ForMember(dest => dest.Group, a => a.MapFrom(src => src.Group))
                    .ReverseMap();

            CreateMap<ItemGroup, ItemGroupVM>().ReverseMap();


            CreateMap<Expense, ExpenseVM>().ReverseMap();
            CreateMap<Income, IncomeVM>().ReverseMap();


            CreateMap<Employee, EmployeeVM>().ReverseMap();

            CreateMap<EmpBonus, EmpBonusVM>().ReverseMap();

            CreateMap<EmpSalary, EmpSalaryVM>().ReverseMap();

            CreateMap<EmpLoan, EmpLoanVM>().ReverseMap();

            CreateMap<Company, CompanyVM>().ReverseMap();
        }
    }
}
